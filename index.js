const express = require("express");

// here in this code "app" - server
const app = express();

const port = 3000;

//Middlewares - software that provides common services and capabilities to applications outside of whats offered by the operating system

// allows your app to read JSON data
app.use(express.json());

//allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));

// [SECTION] ROUTES
// GET method

app.get("/greet", (request, response) => {
	response.send("Hellow from the greet end point");
});

// POST METHOD
app.post("/hello",  (request, response) => {
	response.send(`Hello the ${request.body.firstName} ${request.body.lastName}!`);
})

// Simple registration form

let users = [];

app.post("/signup", (request, response) => {
		if( request.body.username !== '' && request.body.password !== '' ) 
		{
			users.push(request.body);
			response.send(`User ${request.body.username} succesfully registered`);
		}
			else 
		{
			response.send(`Please input both username and password`)
		}
})

// Simple change password

app.patch("/change-password", (request, response) => {
	
	let message;

	for(let i = 0; i < users.length; i ++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;

			break;
		}
		else
		{
			message = "Username not founds"
		}

	}
	response.send(message);
})



app.listen(port, () => console.log(`Server running at ${port}`));

