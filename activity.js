
const express = require("express");
const web = express();
const ports = 4000;
web.use(express.json());
web.use(express.urlencoded({extended: true}));




web.get("/home", (req, res) => {
	res.send("WELCOME TO THE HOMEPAGE");
});


const users = [
  { 
  	username: "virgo",
   	password: "virgo123" 
   },

  { 
  	username: "johndoe", 
  	password: "johndoe123" 
  }
];

web.get('/items', (req, res) => {
  res.json(users);
});



web.delete('/delete-item/:username', (req, res) => {
  const username = req.params.username;
  const index = users.findIndex(user => user.username === username);
  if (index !== -1) {
    users.splice(index, 1);
    res.send(`User ${username} has been deleted`);
  } else {
    res.send(`User ${username} not found`);
  }
});




web.listen(ports, () => console.log(`Server running at ${ports}`));

